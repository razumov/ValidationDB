create table records (rec_id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT, subrec_id TEXT, creation_time REAL, update_time REAL);

create table properties (rec_id INTEGER, name TEXT, value TEXT);

create table plots (rec_id INTEGER, xlabel TEXT, ylabel TEXT, comment TEXT);

create table data (point_id INTEGER PRIMARY KEY AUTOINCREMENT, rec_id INTEGER, xval REAL, yval REAL, xerrm REAL, xerrp REAL, yerrm_stat REAL, yerrp_stat REAL, yerrm_sys REAL, yerrp_sys REAL);

create table cite_records (rec_id INTEGER, cite_id INTEGER);

create table cite (cite_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, author TEXT, journal TEXT, year INTEGER);
