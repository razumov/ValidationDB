#!/usr/bin/env python

import sys
import sqlite3

db = sqlite3.connect(sys.argv[1])
cur = db.cursor()

rec_id = int(sys.argv[2])

if cur.execute("select count(*) from records where rec_id = {0}".format(rec_id)).fetchone()[0] == 0:
  print "# No record {0} found!\n Exiting ...".format(rec_id)
  sys.exit(0)

def getYODA(rec_id):
  if cur.execute("select count(*) from records where rec_id = {0}".format(rec_id)).fetchone()[0] == 0:
    return ""
  yodatxt = ""
  for p in cur.execute("select xval, yval, xerrm, xerrp, yerrm_stat, yerrp_stat from data where rec_id = {0}".format(rec_id)):
    yodatxt += "{0:5e} {1:5e} {2:5e} {3:5e} {4:5e} {5:5e}\n".format(p[0], p[2], p[3], p[1], p[4], p[5])
  return yodatxt.strip()

subr = cur.execute("select subrec_id from records where rec_id = {0}".format(rec_id)).fetchone()[0]
if not subr is None:
  subrecs = subr.strip().split()
if subr is None or len(subrecs) == 0:
  subrecs = [rec_id]


xlabel = cur.execute("select xlabel from plots where rec_id = {0}".format(subrecs[0])).fetchone()[0]
ylabel = cur.execute("select ylabel from plots where rec_id = {0}".format(subrecs[0])).fetchone()[0]

print  """
# BEGIN YODA_SCATTER2D /Record{0}
Path=/Record{0}
Title=Record{0}
Type=Scatter2D
XLabel={1}
YLabel={2}
# xval   xerr-   xerr+   yval    yerr-   yerr+""".format(rec_id, xlabel, ylabel)

for r in subrecs:
  print getYODA(r)

print "# END YODA_SCATTER2D\n"

db.close()


