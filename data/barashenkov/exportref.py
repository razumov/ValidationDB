#!/usr/bin/env python

import re

f = open('barref.tex', 'r')

lines = map(lambda x: x.strip(), f.readlines())
f.close()

items = []
id = 0
name = ""
for l in lines:
  if 'bibitem' in l:
    name = ""
    if l.startswith('%'):
      continue
    id = re.findall('.*{([0-9]+)}.*', l)[0]
    continue
  if not 'vspace' in l:
    name += l
  if 'vspace' in l:
    if id != 0:
      items.append((int(id), name))
      id = 0
      name = ""

for i in items:
  print "insert into cite values ({0}, \"{1}\");".format(i[0],i[1])
